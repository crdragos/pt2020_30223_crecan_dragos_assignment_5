package Presentation;

import Models.MonitoredData;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class FileWritter {

    private static String outputFileName = "Task_";

    public static void generateTextFileForTask1(List<MonitoredData> monitoredDataList) {
        File outputFile = new File(outputFileName + "1.txt");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
            bufferedWriter.write("Monitored activities are: ");
            bufferedWriter.newLine();
            bufferedWriter.newLine();

            for (MonitoredData monitoredData : monitoredDataList) {
                bufferedWriter.write(monitoredData.toString());
                bufferedWriter.newLine();
                bufferedWriter.newLine();
            }

            bufferedWriter.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static void generateTextFileForTask2(int numberOfDistinctDays) {
        File outputFile = new File(outputFileName + "2.txt");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
            bufferedWriter.write("Number of distinct days is: " + numberOfDistinctDays);
            bufferedWriter.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static void generateTextFileForTask3(Map<String, Integer> activities) {
        File outputFile = new File(outputFileName + "3.txt");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
            bufferedWriter.write("How many times each activity has appeared over the entire monitoring period: ");
            bufferedWriter.newLine();
            bufferedWriter.newLine();

            for (String activityName : activities.keySet()) {
                bufferedWriter.write(activityName + " : " + activities.get(activityName));
                bufferedWriter.newLine();
                bufferedWriter.newLine();
            }
            bufferedWriter.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static void generateTextFileForTask4(Map<Integer, Map<String, Integer>> activities) {
        File outputFile = new File(outputFileName + "4.txt");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
            bufferedWriter.write("How many times each activity has appeared for each day over the entire monitoring period: ");
            bufferedWriter.newLine();
            bufferedWriter.newLine();

            int dayCounter = 1;
            for (Integer periodDay : activities.keySet()) {
                bufferedWriter.write("Day " + dayCounter + " (of monitoring period) \n");
                for (String activityName : activities.get(periodDay).keySet()) {
                    bufferedWriter.write(activityName + "(" + activities.get(periodDay).get(activityName) + " appearences) \n");
                }
                dayCounter++;
                bufferedWriter.newLine();
                bufferedWriter.newLine();
            }
            bufferedWriter.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static void generateTextFileForTask5(Map<String, Integer> activities) {
        File outputFile = new File(outputFileName + "5.txt");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
            bufferedWriter.write("For each activity, the entire duration over the monitoring period: ");
            bufferedWriter.newLine();
            bufferedWriter.newLine();

            for (String activityName : activities.keySet()) {
                bufferedWriter.write(activityName + " : " + activities.get(activityName) + " (minutes).");
                bufferedWriter.newLine();
                bufferedWriter.newLine();
            }
            bufferedWriter.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public static void generateTextFileForTask6(List<String> filteredActivities) {
        File outputFile = new File(outputFileName + "6.txt");

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
            bufferedWriter.write("The activities that have more than 90% of the monitoring records with duration less than 5 minutes are: ");
            bufferedWriter.newLine();
            bufferedWriter.newLine();

            for (String activityName : filteredActivities) {
                bufferedWriter.write(activityName);
                bufferedWriter.newLine();
                bufferedWriter.newLine();
            }

            bufferedWriter.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
