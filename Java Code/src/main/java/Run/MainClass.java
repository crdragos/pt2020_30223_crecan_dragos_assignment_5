package Run;

import Models.MonitoredData;
import Models.TaskProcessor;
import Presentation.FileWritter;

import java.util.List;

public class MainClass {
    public static void main(String[] args) {

        TaskProcessor taskProcessor = new TaskProcessor();

        List<MonitoredData> monitoredDataList = taskProcessor.readActivities();

        FileWritter.generateTextFileForTask1(monitoredDataList);
        FileWritter.generateTextFileForTask2(taskProcessor.countDistinctDays(monitoredDataList));
        FileWritter.generateTextFileForTask3(taskProcessor.getActivitiesAppearences(monitoredDataList));
        FileWritter.generateTextFileForTask4(taskProcessor.getActivitiesAppearencesPerDay(monitoredDataList));
        FileWritter.generateTextFileForTask5(taskProcessor.getActivityDuration(monitoredDataList));
        FileWritter.generateTextFileForTask6(taskProcessor.filterActivities(monitoredDataList));
    }
}
