package Models;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class MonitoredData {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;

    public MonitoredData(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }

    public String getActivityLabel() {
        return this.activityLabel;
    }

    public String getStartTime() {
        return startTime.getDayOfMonth() + "/" + startTime.getMonthValue() + "/" + startTime.getYear() + " - " + startTime.getHour() + ":" + startTime.getMinute() + ":" + startTime.getSecond();
    }

    public String getEndTime() {
        return endTime.getDayOfMonth() + "/" + endTime.getMonthValue() + "/" + endTime.getYear() + " - " + endTime.getHour() + ":" + endTime.getMinute() + ":" + endTime.getSecond();
    }

    public long getActivityDuration() {
        return Duration.between(startTime, endTime).toMinutes();
    }

    public String getActivityDate() {
        return startTime.toLocalDate().toString();
    }

    public int getDayOfTheYear() {
        return startTime.getMonthValue() * 31 + startTime.getDayOfMonth();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MonitoredData)) {
            return false;
        }

        if (obj == null) {
            return false;
        }

        MonitoredData monitoredData = (MonitoredData) obj;
        if (this.startTime != monitoredData.startTime || this.endTime != monitoredData.endTime || this.activityLabel != monitoredData.activityLabel) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Activity : " + this.getActivityLabel() + "\n" + "Start time : " + this.getStartTime() + "\n" + "End time : " + this.getEndTime() + "\n" + "Duration : " + this.getActivityDuration() + " (minutes)";
    }
}
