package Models;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskProcessor {

    private static final String inputFile = "Activities.txt";
    private static final Path inputFilePath = Paths.get(inputFile);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public List<MonitoredData> readActivities() {
        try {
            Stream<String> stream = Files.lines(inputFilePath);
            return stream
                    .map(s -> s.split("\\t\\t"))
                    .map(s -> {
                        MonitoredData monitoredData = new MonitoredData(LocalDateTime.parse(s[0], dateTimeFormatter), LocalDateTime.parse(s[1], dateTimeFormatter), s[2].split("\\t")[0]);
                        return monitoredData;
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int countDistinctDays(List<MonitoredData> monitoredDataList) {
//        Stream<MonitoredData> stream = monitoredDataList.stream();
//        return stream
//                .map(MonitoredData::getActivityDate)
//                .distinct()
//                .collect(Collectors.toList())
//                .size();

//        long numberOfDistinctDays =
//                monitoredDataList.stream()
//                .map(MonitoredData::getActivityDate)
//                .distinct()
//                .count();
//        return Integer.parseInt(String.valueOf(numberOfDistinctDays));

        return monitoredDataList
                .stream()
                .map(MonitoredData::getActivityDate)
                .collect(Collectors.toSet())
                .size();
    }

    public Map<String, Integer> getActivitiesAppearences(List<MonitoredData> monitoredDataList) {
        return monitoredDataList
                .stream()
                .collect(
                  Collectors.groupingBy(
                          MonitoredData::getActivityLabel,
                          Collectors.collectingAndThen(
                                  Collectors.mapping(MonitoredData::getActivityLabel, Collectors.counting()),
                                  Long::intValue
                          )
                  )
                );
    }

    public Map<Integer, Map<String, Integer>> getActivitiesAppearencesPerDay(List<MonitoredData> monitoredDataList) {
        return monitoredDataList
                .stream()
                .collect(
                        Collectors.groupingBy(
                                MonitoredData::getDayOfTheYear,
                                Collectors.groupingBy(
                                        MonitoredData::getActivityLabel,
                                        Collectors.collectingAndThen(
                                                Collectors.mapping(MonitoredData::getActivityLabel, Collectors.counting()),
                                                Long::intValue
                                        )
                                )
                        )
                );
    }

    public Map<String, Integer> getActivityDuration(List<MonitoredData> monitoredDataList) {
        return monitoredDataList
                .stream()
                .collect(
                        Collectors.groupingBy(
                                MonitoredData::getActivityLabel,
                                Collectors.collectingAndThen(
                                        Collectors.mapping(MonitoredData::getActivityDuration, Collectors.summingInt(Long::intValue)),
                                        Integer::intValue
                                )
                        )
                );
    }

    public List<String> filterActivities(List<MonitoredData> monitoredDataList) {
        return monitoredDataList
                .stream()
                .collect(
                  Collectors.groupingBy(
                          MonitoredData::getActivityLabel,
                          Collectors.toSet()
                  )
                )
                .entrySet()
                .stream()
                .map(activity -> {
                    int howManyTimesDurationsIsLessThanFive = 0;
                    for (MonitoredData monitoredData : activity.getValue()) {
                        if (monitoredData.getActivityDuration() < 5) {
                            howManyTimesDurationsIsLessThanFive++;
                        }
                    }

                    if (howManyTimesDurationsIsLessThanFive >= activity.getValue().size() * 90 / 100) {
                        return activity.getKey();
                    }

                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
